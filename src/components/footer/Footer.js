import React from 'react';
import { PropTypes } from 'prop-types';

import SocialLinks from '../common/social-links';

const Footer = ({ title, signOff, footerTagline }) => (
  <section id="footer" className="container text-center padding-lg lh-md font--16">
    <strong>
      <div dangerouslySetInnerHTML={{ __html: signOff || '' }} />
    </strong>

    <br />
    <br />

    <SocialLinks />

    <button
      type="button"
      style={{
        color: '#666',
        border: 'none',
        position: 'absolute',
        bottom: 5,
        right: -15,
        fontSize: 17,
      }}
      onClick={() => console.info(`
      __.-._
      '-._"7'
      /'.-c
      |  /T
     _)_/LI

     Do or do not. There is no try.

      We honestly can't thank you enough for checking out our site.
      The fact you're even digging in here is pretty neat.

      Are you a non-for profit? We love working with them and are totally willing to work with your budget. Just drop us a line.

      Be excellent to each other
        -Bill S. Preston, Esq.
      `)} // eslint-disable-line
    >
      &#960;
    </button>

    <br />
    <br />

    <small className="font--text">
      {`copyright © ${new Date().getFullYear()} ${title.toLowerCase()}`}
      <br />
      <span className="font--text opacity8">
        {footerTagline}
      </span>
    </small>
  </section>
);

Footer.propTypes = {
  title: PropTypes.string.isRequired,
  signOff: PropTypes.string.isRequired,
  footerTagline: PropTypes.string.isRequired,
};

export default Footer;